package com.cookies.twocustomselector;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity {
    private final static String TAG = MainActivity.class.getSimpleName();
    TwoCircleSelector selector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        selector = (TwoCircleSelector) findViewById(R.id.selector);
        selector.setmOnSeekChangeListener(new TwoCircleSelector.OnSeekChangeListener() {
            @Override
            public void onStartTrackingTouch() {
                Log.d(TAG, "Start.");
            }

            @Override
            public void onUpdateTouch() {
                Log.d(TAG, "Update from: " + selector.getProgressFrom()+" before: " + selector.getProgressBefore());
            }

            @Override
            public void onStopTackingTouch() {
                Log.d(TAG, "Stop.");
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
