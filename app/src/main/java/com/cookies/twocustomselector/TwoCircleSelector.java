package com.cookies.twocustomselector;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TwoCircleSelector extends View {

    private final static String TAG = TwoCircleSelector.class.getSimpleName();

    private Context context;
    int arcColor = getResources().getColor(R.color.blue_light);
    int backroundColor = getResources().getColor(R.color.blue_dark);

    private RectF mArcRect = new RectF();
    private Paint mArcPaint;
    private int mCenterX;
    private int mCenterY;
    private int mAcrWidth = 7;
    private int mArcSweep = 300;
    private int mArcStart = -80;
    private int mArcRadius;
    private int width;
    private int height;

    /*private int scaleMax=90;
    private int minNumber=15;
    private int maxNumber=60;
    private int startFrom=15;
    private int startBefore=70;
    private int interval=15;*/

    private int minNumber=3;
    private int maxNumber=28;
    private int startFrom=3;
    private int startBefore=20;
    private int intervalMin=3;
    private int intervalMax=8;

    private boolean touch = false;
    private Selector touchSelector = null;

    private Selector selectorFrom;
    private Selector selectorBefore;

    private OnSeekChangeListener mOnSeekChangeListener;

    public interface OnSeekChangeListener{
        void onStartTrackingTouch();
        void onUpdateTouch();
        void onStopTackingTouch();
    }

    public TwoCircleSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        setBackgroundColor(backroundColor);
        mArcPaint = new Paint();
        mArcPaint.setColor(arcColor);
        mArcPaint.setAntiAlias(true);
        mArcPaint.setStyle(Paint.Style.STROKE);
        mArcPaint.setStrokeCap(Paint.Cap.ROUND);
        final float scale = context.getResources().getDisplayMetrics().density;
        int arcWidth = (int) (mAcrWidth * scale + 0.5f);
        mArcPaint.setStrokeWidth(arcWidth);

        selectorFrom = new Selector(context, 24, startFrom, arcColor, backroundColor, mArcStart, mAcrWidth, 60, minNumber, maxNumber);
        selectorBefore = new Selector(context, 32, startBefore, arcColor, backroundColor, mArcStart, mAcrWidth, 80, minNumber, maxNumber);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Draw the arcs
        canvas.drawArc(mArcRect, mArcStart, mArcSweep, false, mArcPaint);

        selectorFrom.draw(canvas);
        selectorBefore.draw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        height = getDefaultSize(getSuggestedMinimumHeight(),
                heightMeasureSpec);
        width = getDefaultSize(getSuggestedMinimumWidth(),
                widthMeasureSpec);
        int min = Math.min(width, height);
        float top = 0;
        float left = 0;
        int arcDiameter = 0;

        mCenterX = (int) (width * 0.5f);
        mCenterY = (int) (height * 0.5f);

        arcDiameter = min - getPaddingLeft() - (int) (min * 0.35f);

        mArcRadius = arcDiameter / 2;
        top = height / 2 - (arcDiameter / 2);
        left = width / 2 - (arcDiameter / 2);
        mArcRect.set(left, top, left + arcDiameter, top + arcDiameter);


        //float arcStart = mArcStart;
        //set angle from selectorFrom
        selectorFrom.setProgressFromAngle(startFrom);
        selectorFrom.setX(mCenterX + (int) (mArcRadius * Math.cos(Math.toRadians(selectorFrom.getmAngle()))));
        selectorFrom.setY(mCenterY + (int) (mArcRadius * Math.sin(Math.toRadians(selectorFrom.getmAngle()))));

        //arcStart=215;
        selectorBefore.setProgressFromAngle(startBefore);
        selectorBefore.setX(mCenterX + (int) (mArcRadius * Math.cos(Math.toRadians(selectorBefore.getmAngle()))));
        selectorBefore.setY(mCenterY + (int) (mArcRadius * Math.sin(Math.toRadians(selectorBefore.getmAngle()))));

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // Start click to canvas
                onStartTrackingTouch(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                updateOnTouch(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_UP:
                onStopTrackingTouch();
                //setPressed(false);
                break;
            case MotionEvent.ACTION_CANCEL:
                onStopTrackingTouch();
                //setPressed(false);
                break;
        }
        return true;
    }

    private void onStopTrackingTouch() {
        if (touch) touch = false;
        else return;
        if (mOnSeekChangeListener != null) {
            mOnSeekChangeListener.onStopTackingTouch();
        }

    }

    private void onStartTrackingTouch(float x, float y) {
        touch = true;
        touchSelector = getSelectorFromCoordinates(x, y);
        if (touchSelector == null) touch = false;
        else {
            if (mOnSeekChangeListener != null) {
                mOnSeekChangeListener.onStartTrackingTouch();
            }
        }

    }

    private void updateOnTouch(float eventX, float eventY) {

        if (!touch) return;
        if (touchSelector == null) return;
        Selector selector = touchSelector;

        float mTouchAngle = getTouchDegrees(selector, eventX, eventY);

        int newX = mCenterX + (int) (mArcRadius * Math.cos(Math.toRadians(mTouchAngle)));
        int newY = mCenterY + (int) (mArcRadius * Math.sin(Math.toRadians(mTouchAngle)));

        selector.setX(newX);
        selector.setY(newY);

        int percentLast = selector.getProgress();
        float angleLast = selector.getmAngle();

        selector.setmAngle(mTouchAngle);
        selector.setProgressFromAngle(mTouchAngle);

        if (selector.getProgress()>maxNumber||selector.getProgress()<minNumber){
            selector.setProgress(percentLast);
            selector.setmAngle(angleLast);
            return;
        }

        if (Math.abs(selector.getProgress()-percentLast)>3){
            selector.setProgress(percentLast);
            selector.setmAngle(angleLast);
            return;
        }

        if (mOnSeekChangeListener != null) {
            mOnSeekChangeListener.onUpdateTouch();
        }

        if (selector == selectorFrom) {
            if (selectorFrom.getProgress() + intervalMin >= selectorBefore.getProgress()) {
                int progressBefore = selectorBefore.getProgress();
                float angleBefore = selectorBefore.getmAngle();
                selectorBefore.setProgress(selectorBefore.getProgress() + (int) Math.abs(selectorFrom.getProgress() - percentLast));
                if (selectorBefore.getProgress()>maxNumber||selectorBefore.getProgress()<0){
                    selectorBefore.setProgress(progressBefore);
                    selectorBefore.setmAngle(angleBefore);
                    selector.setProgress(percentLast);
                    selector.setmAngle(angleLast);
                    return;
                }
                int X = mCenterX + (int) (mArcRadius * Math.cos(Math.toRadians(selectorBefore.getmAngle())));
                int Y = mCenterY + (int) (mArcRadius * Math.sin(Math.toRadians(selectorBefore.getmAngle())));
                selectorBefore.setX(X);
                selectorBefore.setY(Y);
                invalidate();
                return;
            }

            if (selectorFrom.getProgress() + intervalMax < selectorBefore.getProgress()) {
                selectorBefore.setProgress(selectorBefore.getProgress()-(int)Math.abs(selectorFrom.getProgress()-percentLast));
                int X = mCenterX + (int) (mArcRadius * Math.cos(Math.toRadians(selectorBefore.getmAngle())));
                int Y = mCenterY + (int) (mArcRadius * Math.sin(Math.toRadians(selectorBefore.getmAngle())));
                selectorBefore.setX(X);
                selectorBefore.setY(Y);
                invalidate();
                return;
            }
        }

        if (selector == selectorBefore) {
            if (selectorBefore.getProgress() - intervalMin <= selectorFrom.getProgress()) {
                int progressFrom = selectorFrom.getProgress();
                float angleFrom = selectorFrom.getmAngle();
                selectorFrom.setProgress(selectorFrom.getProgress() - (int) Math.abs(selectorBefore.getProgress() - percentLast));
                if (selectorFrom.getProgress()>maxNumber||selectorFrom.getProgress()<0){
                    selectorFrom.setProgress(progressFrom);
                    selectorFrom.setmAngle(angleFrom);
                    selector.setProgress(percentLast);
                    selector.setmAngle(angleLast);
                    return;
                }
                int X = mCenterX + (int) (mArcRadius * Math.cos(Math.toRadians(selectorFrom.getmAngle())));
                int Y = mCenterY + (int) (mArcRadius * Math.sin(Math.toRadians(selectorFrom.getmAngle())));
                selectorFrom.setX(X);
                selectorFrom.setY(Y);
                invalidate();
                return;
            }

            if (selectorBefore.getProgress() - intervalMax > selectorFrom.getProgress()) {
                selectorFrom.setProgress(selectorFrom.getProgress() + (int) Math.abs(selectorBefore.getProgress() - percentLast));
                int X = mCenterX + (int) (mArcRadius * Math.cos(Math.toRadians(selectorFrom.getmAngle())));
                int Y = mCenterY + (int) (mArcRadius * Math.sin(Math.toRadians(selectorFrom.getmAngle())));
                selectorFrom.setX(X);
                selectorFrom.setY(Y);
            }
        }

        invalidate();
    }

    private float getTouchDegrees(Selector selector, float xPos, float yPos) {

        float x = xPos - mCenterX;
        x = -x;
        float y = yPos - mCenterY;
        double angle = Math.toDegrees(Math.atan2(x, y) + (Math.PI / 2)
                - Math.toRadians(0));

        if (angle > 220 || angle < -80) angle = selector.getmAngle();

        return (float) angle;
    }

    private Selector getSelectorFromCoordinates(float x, float y) {

        if (isTouch(selectorFrom, x, y)) return selectorFrom;
        if (isTouch(selectorBefore, x, y)) return selectorBefore;

        return null;
    }

    private boolean isTouch(Selector selector, float xPos, float yPos) {
        float x1, y1, x2, y2;

        x1 = selector.getX();
        y1 = selector.getY();

        x2 = selector.getWidth() + x1;
        y2 = selector.getHeight() + y1;

        x1 = x1 - selector.getWidth();
        y1 = y1 - selector.getHeight();

        boolean isTouch = false;

        if (xPos >= x1 && yPos >= y1 && xPos <= x2 && yPos <= y2) isTouch = true;

        return isTouch;
    }

    public void setmOnSeekChangeListener(OnSeekChangeListener listener) {
        mOnSeekChangeListener = listener;
    }

    public int getProgressFrom(){
        return selectorFrom.getProgress();
    }

    public int getProgressBefore(){
        return selectorBefore.getProgress();
    }

}

class Selector {
    public String TAG = Selector.class.getSimpleName();
    private Context context;
    private float width = 0;
    private float height = 0;
    private TextView textView;
    private LinearLayout layout;
    private int progress = 0;
    private int x;
    private int y;
    private float mAngle = 0;
    private int mArcStart;
    private int scaleMin=0;
    private int scaleMax=100;
    private int fieldValues=100;

    public Selector(Context context, int textSize, int progress, int color, int backgroundColor, int mArcStart,
                    int strokeWidth, int diameter, int scaleMin, int ScaleMax) {
        this.context = context;
        this.progress = progress;
        this.mArcStart = mArcStart;
        this.scaleMin = scaleMin;
        this.scaleMax = scaleMax;
        fieldValues = scaleMax-scaleMin;

        // prepare
        final float scale = context.getResources().getDisplayMetrics().density;
        int width = (int) (diameter * scale + 0.5f);
        int height = (int) (diameter * scale + 0.5f);

        strokeWidth = (int) (strokeWidth * scale + 0.5F); // 3px not dp
        int roundRadius = 15; // 8px not dp

        GradientDrawable gd = new GradientDrawable();
        gd.setColor(backgroundColor);
        gd.setCornerRadius(roundRadius);
        gd.setStroke(strokeWidth, color);
        gd.setShape(GradientDrawable.OVAL);
        gd.setSize(width, height);

        textView = new TextView(context);
        textView.setTextColor(color);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= 16) {
            textView.setBackground(gd);
        } else {
            textView.setBackgroundDrawable(gd);
        }

        //textView.setBackgroundResource(R.drawable.big_selector);
        textView.setTextSize(textSize);
        textView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        textView.setVisibility(View.VISIBLE);
        layout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new
                LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layout.setLayoutParams(layoutParams);
        layout.addView(textView);

        setProgress(progress);
    }

    public void setProgressFromAngle(float angle) {
        mAngle = angle;
        angle -= mArcStart;
        float percent = 360 / fieldValues;
        progress = (int) (angle / percent);
        textView.setText("" + progress);
    }

    public void setProgress(int progress) {
        float percent = 360 / fieldValues;
        this.progress = progress;
        mAngle = progress * percent;
        Log.d(TAG, "mAngle: " + mAngle);
        mAngle += mArcStart;
        Log.d(TAG, "mAngle: " + mAngle);
        textView.setText("" + progress);
    }

    public void draw(Canvas canvas) {
        canvas.save();
        layout.measure(canvas.getWidth(), canvas.getHeight());
        layout.layout(0, 0, canvas.getWidth(), canvas.getHeight());
        float mX = x - getWidth() / 2;
        float mY = y - getHeight() / 2;
        canvas.translate(mX, mY);
        layout.draw(canvas);
        canvas.restore();
    }

    public float getWidth() {
        width = textView.getMeasuredWidth();
        return width;
    }

    public float getHeight() {
        height = textView.getMeasuredHeight();
        return height;
    }

    public int getProgress() {
        return progress;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public float getmAngle() {
        return mAngle;
    }

    public void setmAngle(float mAngle) {
        this.mAngle = mAngle;
    }

}
